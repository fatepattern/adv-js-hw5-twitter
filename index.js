fetch("https://ajax.test-danit.com/api/json/users", {
        method: 'GET'
})
  .then(response => response.json())
  .then(json => {
    const userRenderer = new UserRenderer(json);
    userRenderer.showUsers();
    getPosts()
  })


function getPosts(){
  fetch("https://ajax.test-danit.com/api/json/posts/", {
        method: 'GET'
})
  .then(response => response.json())
  .then(json => showPosts(json))
}


class UserRenderer {
  constructor(data) {
    this.data = data;
    this.root = document.querySelector(".root");
  }

  renderUser(user) {
    const poster = document.createElement('div');
    poster.classList.add("poster");

    const posterInfo = document.createElement('div');
    poster.appendChild(posterInfo);
    posterInfo.classList.add("poster__info");
    poster.setAttribute("id", user.id);

    const name = document.createElement('h2');
    posterInfo.appendChild(name);

    const email = document.createElement('p');
    email.classList.add("poster__email");
    posterInfo.appendChild(email);

    name.innerText = `${user.name}`;
    email.innerText = `${user.email}`;

    this.root.appendChild(poster);
  }

  showUsers() {
    this.data.forEach(user => {
      this.renderUser(user);
    });
  }
}

function showPosts(data){

  let posters = document.querySelectorAll(".poster");

  for(let i = 0; i < data.length; i++){
    let post = document.createElement('div');
    post.classList.add("post");
    post.setAttribute("id", data[i].userId);
    post.setAttribute("data-postId", data[i].id);
   
    let title = document.createElement('h4');
    title.classList.add("post-title");
    post.appendChild(title);

    let body = document.createElement('p');
    body.classList.add("post-body");
    post.appendChild(body);

    let button = document.createElement('button');
    button.setAttribute("id", data[i].id);
    button.classList.add('delete-btn')
    button.innerText = 'Delete';
    post.appendChild(button);
    title.innerText = `${data[i].title}`;
    body.innerText = `${data[i].body}`

    posters.forEach((poster) => {
      if(poster.id === post.id){
        poster.appendChild(post)
      }
    })
  }
  
  
  let delButton = document.querySelectorAll('.delete-btn');
  
  delButton.forEach((button) => {

   button.addEventListener("click", function(){
    
    let allPosts = document.querySelectorAll('.post')
    console.log(button.id);

    allPosts.forEach((post) => {
      if(post.dataset.postid === button.id){
        fetch(`https://ajax.test-danit.com/api/json/posts/${post.dataset.postid}`, {
          method: 'DELETE'
        })
        .then(response => {
          if(response.ok){
            post.remove();
          } else {
            console.error('Failed to delete the card');
          }
        })
        .catch(error => {
          console.error('Error occurred while deleting the card:', error);
        });
      }
     })


   })
  })
}

